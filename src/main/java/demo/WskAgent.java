package demo;

import com.far.net.server.session.Session;
import com.far.net.interf.IWskAgent;

public class WskAgent implements IWskAgent {

    Session session;

    public Session getSession() {
        return session;
    }


    @Override
    public String getSessionId() {
        return this.session.getSeesionId();
    }

    @Override
    public IWskAgent setSession(Session e) {
        this.session = e;
        return this;
    }

    @Override
    public void sendMessage(String message) {
        this.session.sendText(message);
    }

    @Override
    public Object getVisitor() {
        return null;
    }


}
