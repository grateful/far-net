package demo;


import com.far.net.server.session.Session;
import com.far.net.interf.IWskAgent;
import com.far.net.interf.IWskHandler;
import com.far.net.server.core.FarNetServer;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


public class Startup {


    @PostConstruct
    public void init() {
        this.run();
    }

    public void run() {

        new FarNetServer("*:9099:wsk", new IWskHandler() {
            @Override
            public IWskAgent onOpen(Session e) {
                return new WskAgent().setSession(e);
            }



            @Override
            public void onClose(String id) {

            }

            @Override
            public void onMessage(String id, String message) {

            }

            @Override
            public void onNetInitComplete(Session e) {

            }


            @Override
            public void onError(String id,Throwable throwable) {

            }


        }).start();
    }


}
