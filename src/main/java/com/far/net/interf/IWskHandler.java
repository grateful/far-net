package com.far.net.interf;

import com.far.net.server.session.Session;

public interface IWskHandler<T> {

    IWskAgent<T> onOpen(Session e);

//    void onClose(IWskAgent<T> e);
//    void onMessage(IWskAgent<T> e, String message);
//    void onNetInitComplete(IWskAgent<T> e);
//    void onError(IWskAgent<T> e);
    //不需要AGENT管理的就使用这个方法
    void onClose(String  id);
    void onMessage(String  id, String message);
    void onNetInitComplete(Session e);
    //不需要AGENT管理的就使用这个方法
    void onError(String  id,Throwable throwable);

}
