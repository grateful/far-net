package com.far.net.interf;

import com.far.net.server.session.Session;

public interface IProcessAgent<T> {
    String getSessionId();
    IProcessAgent<T> setSession(Session e);
    void sendMessage(String message);

    void sendBytes(byte[] data);
    //获取访问者
    T getVisitor();
}
