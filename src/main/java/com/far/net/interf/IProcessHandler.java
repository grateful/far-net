package com.far.net.interf;

import com.far.net.server.session.Session;
import io.netty.buffer.ByteBuf;

import java.nio.Buffer;

public interface IProcessHandler<T> {

    IProcessAgent<T> onOpen(Session e);

    void onClose(IProcessAgent<T> e);
    void onMessage(IProcessAgent<T> e, String message);
    void onMessage(String id, ByteBuf byteBuf);
    void onNetInitComplete(IProcessAgent<T> e);
    void onError(IProcessAgent<T> e);
    //不需要AGENT管理的就使用这个方法
    void onClose(String  id);
    void onMessage(String  id, String message);
    void onNetInitComplete(Session e);
    //不需要AGENT管理的就使用这个方法
    void onError(String  id,Throwable e);


}
