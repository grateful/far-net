package com.far.net.interf;

import com.far.net.server.session.Session;

public interface IWskAgent<T> {
    String getSessionId();
    IWskAgent<T> setSession(Session e);
    void sendMessage(String message);
    //获取访问者
    T getVisitor();
}
