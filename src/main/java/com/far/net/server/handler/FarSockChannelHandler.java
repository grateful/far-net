package com.far.net.server.handler;

import com.far.net.interf.IProcessAgent;
import com.far.net.interf.IProcessHandler;
import com.far.net.interf.IWskAgent;
import com.far.net.interf.IWskHandler;
import com.far.net.server.session.Session;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.rmi.server.ExportException;
import java.util.logging.Logger;


public class FarSockChannelHandler extends ChannelInboundHandlerAdapter {
    //scoket的连接名字
    private Logger logger = Logger.getLogger(FarSockChannelHandler.class.getName());
    private WebSocketServerHandshaker handshaker;
    private IProcessHandler<?> processHandler;

    public IProcessHandler<?> getProcessHandler() {
        return processHandler;
    }

    public FarSockChannelHandler setProcessHandler(IProcessHandler<?> processHandler) {
        this.processHandler = processHandler;
        return this;
    }

    private String url;

    public String getUrl() {
        return url;
    }

    public FarSockChannelHandler setUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * s连接异常 需要关闭相关资源
     * 需要参考的资料
     * 注意！目前异常不能再次调用断开，否则会重复，
     * 可以专门开发一个异常就断开连接的方法!
     * https://blog.csdn.net/abc3224302/article/details/81502640
     *
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        getProcessHandler().onError(ctx.channel().id().toString(),cause);
    }

    /**
     * s活跃的通道 也可以当作用户连接上客户端进行使用
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        Session session = new Session();
        session.setCtx(ctx);
        //获取外部的agent
        IProcessAgent<?> agent = getProcessHandler().onOpen(session);
    }

    /**
     * s不活跃的通道 就说明用户失去连接
     * <p>
     * ChannelOption.SO_KEEPALIVE 该参数为TRUE时，此方法才有用？待验证
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        String ctxId = ctx.channel().id().toString();
        getProcessHandler().onClose(ctxId);
    }

    /**
     * s 这里只要完成 flush
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    /**
     * s 这里是保持服务器与客户端长连接 进行心跳检测 避免连接断开
     * Exception文件到本地播放
     *
     * @param ctx
     * @param evt
     * @throws
     */
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {

        if (evt instanceof IdleStateEvent) {
            //	LOGGER.debug("userEventTriggered IdleStateEvent 被触发了。");
            IdleStateEvent stateEvent = (IdleStateEvent) evt;
            PingWebSocketFrame ping = new PingWebSocketFrame();
            ByteBuf buf = Unpooled.copiedBuffer("fast-net-ping", CharsetUtil.UTF_8);
            ping.content().writeBytes(buf);
            buf.release();
            switch (stateEvent.state()) {

                // 读空闲（服务器端）
                // 服务器已经在指定的时间段内，没有读取CTX的消息了，
                // 换句话说 就是CTX没有在指定的时间内发送消息过来了

                case READER_IDLE:
                    //LogUtils.info("NettyServer", "【userEventTriggered】====>" +"【" + ctx.channel().remoteAddress() + "】读空闲（服务器端）");
                    //发送一个PING消息，要求客户端也发个PING消息过来
                    ctx.writeAndFlush(ping);
                    break;
                // 写空闲（客户端）

                case WRITER_IDLE:
                    //LOGGER.info("【" + ctx.channel().remoteAddress() + "】写空闲（客户端）");
                    //发送一个PING消息，要求客户端也发个PING消息过来
                    ctx.writeAndFlush(ping);
                    break;
                case ALL_IDLE:
                    //LOGGER.info("【" + ctx.channel().remoteAddress() + "】读写空闲");
                    ctx.writeAndFlush(ping);
                    break;
            }
            //交给父类处理
            super.userEventTriggered(ctx, evt);
        }
    }


    public void closeCtx(ChannelHandlerContext ctx) {
        ctx.close();
        ctx.channel().close();
    }


    /**
     * s收发消息处理
     * https://stackoverflow.com/questions/34634750/netty-getting-an-exceptioncaught-event-was-fired-and-it-reached-at-the-tail-o
     *
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        try {
            if (msg instanceof HttpRequest) {
                doHandlerHttpRequest(ctx, (HttpRequest) msg);
            } else if (msg instanceof WebSocketFrame) {
                doHandlerWebSocketFrame(ctx, (WebSocketFrame) msg);
            } else {
                ByteBuf byteBuf = (ByteBuf) msg;
                processHandler.onMessage(ctx.channel().id().toString(), byteBuf);
            }
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * websocket消息处理
     *
     * @param ctx
     * @param msg
     */
    private void doHandlerWebSocketFrame(ChannelHandlerContext ctx, WebSocketFrame msg) {

        // 判断msg 是哪一种类型 分别做出不同的反应
        if (msg instanceof CloseWebSocketFrame) {
            String ctxId = ctx.channel().id().toString();
            //此处会触发 handlerRemoved
            handshaker.close(ctx.channel(), (CloseWebSocketFrame) msg);

            return;
        }
        if (msg instanceof PongWebSocketFrame) {
            PongWebSocketFrame pong = new PongWebSocketFrame(msg.content().retain());
            ctx.writeAndFlush(pong);
            return;
        }
        if (msg instanceof PingWebSocketFrame) {
            PingWebSocketFrame ping = new PingWebSocketFrame(msg.content().retain());
            ctx.writeAndFlush(ping);
            return;
        }
        if (!(msg instanceof TextWebSocketFrame)) {
            throw new UnsupportedOperationException("不支持二进制");
        }
        String baseMessage = ((TextWebSocketFrame) msg).text();
        getProcessHandler().onMessage(ctx.channel().id().toString(), baseMessage);

    }

    /**
     * wetsocket第一次连接握手
     *
     * @param ctx
     * @param msg
     */
    private void doHandlerHttpRequest(ChannelHandlerContext ctx, HttpRequest msg) throws Exception {
        // http 解码失败
        if (!msg.decoderResult().isSuccess() || (!"websocket".equals(msg.headers().get("Upgrade")))) {
            sendHttpResponse(ctx, (FullHttpRequest) msg,
                    new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST));
        }
        // 可以获取msg的uri来判断
        String uri = msg.uri();
        if (!uri.substring(1).equals(this.url)) {
            ctx.close();
            //获取到的标志和设定的标志不一样
            throw new Exception(String.format("require flag %s  got %s , The access flag obtained is different from the one set", uri, this.url));

        }


        // 可以通过url获取其他参数
        WebSocketServerHandshakerFactory factory = new WebSocketServerHandshakerFactory(
                "ws://" + msg.headers().get("Host") + "/" + this.url + "", null, false);
        handshaker = factory.newHandshaker(msg);
        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        }
        // 进行连接
        handshaker.handshake(ctx.channel(), (FullHttpRequest) msg);
        // 可以做其他处理
        //真正的初始化完成，服务器可以直接发消息，是从这儿开始!!!
//        ctx.writeAndFlush(new TextWebSocketFrame("asdasd"));

        Session session = new Session();
        session.setCtx(ctx);
        getProcessHandler().onNetInitComplete(session);


    }

    private static void sendHttpResponse(ChannelHandlerContext ctx, FullHttpRequest req, DefaultFullHttpResponse res) {
        // 返回应答给客户端
        if (res.status().code() != 200) {
            ByteBuf buf = Unpooled.copiedBuffer(res.status().toString(), CharsetUtil.UTF_8);
            res.content().writeBytes(buf);
            buf.release();
        }
        // 如果是非Keep-Alive，关闭连接
        ChannelFuture f = ctx.channel().writeAndFlush(res);
        if (!HttpUtil.isKeepAlive(req) || res.status().code() != 200) {
            f.addListener(ChannelFutureListener.CLOSE);
        }
    }

}
