package com.far.net.server.handler;

import com.far.net.interf.IWskHandler;
import com.far.net.server.session.Session;
import com.far.net.interf.IWskAgent;
import com.far.net.server.core.FarNetServer;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;

/**
 * netty发送和接收数据handler处理器 主要是继承 SimpleChannelInboundHandler 和 ChannelInboundHandlerAdapter
 * <p>
 * 　　一般用netty来发送和接收数据都会继承SimpleChannelInboundHandler和ChannelInboundHandlerAdapter这两个抽象类，那么这两个到底有什么区别呢？
 * <p>
 * 　　其实用这两个抽象类是有讲究的，在客户端的业务Handler继承的是SimpleChannelInboundHandler，而在服务器端继承的是ChannelInboundHandlerAdapter。
 * <p>
 * 　　最主要的区别就是SimpleChannelInboundHandler在接收到数据后会自动release掉数据占用的Bytebuffer资源(自动调用Bytebuffer.release())。
 * 而为何服务器端不能用呢，因为我们想让服务器把客户端请求的数据发送回去，而服务器端有可能在channelRead方法返回前还没有写完数据，因此不能让它自动release。
 * <p>
 * <p>
 * ref https://phubing.blog.csdn.net/article/details/85004424?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.no_search_link&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.no_search_link
 *
 * @author Mike@Fang.j
 * @ClassName: RadarChannelHandler
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019年1月7日
 */
public class FarChannelHandler extends ChannelInboundHandlerAdapter {
    //scoket的连接名字

    private WebSocketServerHandshaker handshaker;

    private IWskHandler<?> wskHandler;

    public IWskHandler<?> getWskHandler() {
        return wskHandler;
    }

    public FarChannelHandler setWskHandler(IWskHandler<?> wskHandler) {
        this.wskHandler = wskHandler;
        return this;
    }

    private String url;

    public String getUrl() {
        return url;
    }

    public FarChannelHandler setUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * s连接异常 需要关闭相关资源
     * 需要参考的资料
     * 注意！目前异常不能再次调用断开，否则会重复，
     * 可以专门开发一个异常就断开连接的方法!
     * https://blog.csdn.net/abc3224302/article/details/81502640
     *
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        wskHandler.onError(ctx.channel().id().toString(),cause);
    }

    /**
     * s活跃的通道 也可以当作用户连接上客户端进行使用
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        Session session = new Session();
        session.setCtx(ctx);
        //获取外部的agent
        IWskAgent<?> agent = wskHandler.onOpen(session);
    }

    /**
     * s不活跃的通道 就说明用户失去连接
     * <p>
     * ChannelOption.SO_KEEPALIVE 该参数为TRUE时，此方法才有用？待验证
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        String ctxId = ctx.channel().id().toString();
        wskHandler.onClose(ctxId);
    }

    /**
     * s 这里只要完成 flush
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    /**
     * s 这里是保持服务器与客户端长连接 进行心跳检测 避免连接断开
     * Exception文件到本地播放
     *
     * @param ctx
     * @param evt
     * @throws
     */
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {

        if (evt instanceof IdleStateEvent) {
            //	LOGGER.debug("userEventTriggered IdleStateEvent 被触发了。");
            IdleStateEvent stateEvent = (IdleStateEvent) evt;
            PingWebSocketFrame ping = new PingWebSocketFrame();
            ByteBuf buf = Unpooled.copiedBuffer("fast-net-ping", CharsetUtil.UTF_8);
            ping.content().writeBytes(buf);
            buf.release();
            switch (stateEvent.state()) {

                // 读空闲（服务器端）
                // 服务器已经在指定的时间段内，没有读取CTX的消息了，
                // 换句话说 就是CTX没有在指定的时间内发送消息过来了

                case READER_IDLE:
                    //LogUtils.info("NettyServer", "【userEventTriggered】====>" +"【" + ctx.channel().remoteAddress() + "】读空闲（服务器端）");
                    //发送一个PING消息，要求客户端也发个PING消息过来
                    ctx.writeAndFlush(ping);
                    break;
                // 写空闲（客户端）

                case WRITER_IDLE:
                    //LOGGER.info("【" + ctx.channel().remoteAddress() + "】写空闲（客户端）");
                    //发送一个PING消息，要求客户端也发个PING消息过来
                    ctx.writeAndFlush(ping);
                    break;
                case ALL_IDLE:
                    //LOGGER.info("【" + ctx.channel().remoteAddress() + "】读写空闲");
                    ctx.writeAndFlush(ping);
                    break;
            }
            //交给父类处理
            super.userEventTriggered(ctx, evt);
        }
    }


    public void closeCtx(ChannelHandlerContext ctx) {
        ctx.close();
        ctx.channel().close();
    }


    /**
     * s收发消息处理
     * https://stackoverflow.com/questions/34634750/netty-getting-an-exceptioncaught-event-was-fired-and-it-reached-at-the-tail-o
     *
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        if (msg instanceof HttpRequest) {
            doHandlerHttpRequest(ctx, (HttpRequest) msg);
        } else if (msg instanceof WebSocketFrame) {
            doHandlerWebSocketFrame(ctx, (WebSocketFrame) msg);
        }

        //201917新增
        // ctx.fireChannelRead(msg);
    }

    /**
     * websocket消息处理
     *
     * @param ctx
     * @param msg
     */
    private void doHandlerWebSocketFrame(ChannelHandlerContext ctx, WebSocketFrame msg) {

        // 判断msg 是哪一种类型 分别做出不同的反应
        if (msg instanceof CloseWebSocketFrame) {
            String ctxId = ctx.channel().id().toString();
            //此处会触发 handlerRemoved
            handshaker.close(ctx.channel(), (CloseWebSocketFrame) msg);
            //以下代码会在channelInactive 执行 不用在写
//			LOGGER.debug("【执行链接断线】CloseWebSocketFrame");
//			
//			BeanBundle.getInstance().getConnectManage().closeConnect(ctxId);
//			closeCtx(ctx);
            return;
        }

        if (msg instanceof PongWebSocketFrame) {
            PongWebSocketFrame pong = new PongWebSocketFrame(msg.content().retain());
            ctx.writeAndFlush(pong);
            return;
        }

        if (msg instanceof PingWebSocketFrame) {
            PingWebSocketFrame ping = new PingWebSocketFrame(msg.content().retain());
            ctx.writeAndFlush(ping);
            return;
        }

        if (!(msg instanceof TextWebSocketFrame)) {
            throw new UnsupportedOperationException("不支持二进制");
        }
        String baseMessage = ((TextWebSocketFrame) msg).text();

        wskHandler.onMessage(ctx.channel().id().toString(),baseMessage);

    }

    /**
     * wetsocket第一次连接握手
     *
     * @param ctx
     * @param msg
     */
    private void doHandlerHttpRequest(ChannelHandlerContext ctx, HttpRequest msg) throws Exception {
        // http 解码失败
        if (!msg.decoderResult().isSuccess() || (!"websocket".equals(msg.headers().get("Upgrade")))) {
            sendHttpResponse(ctx, (FullHttpRequest) msg,
                    new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST));
        }
        // 可以获取msg的uri来判断
        String uri = msg.uri();
        if (!uri.substring(1).equals(this.url)) {
            ctx.close();
            //获取到的标志和设定的标志不一样
            throw new Exception(String.format("require flag %s  got %s , The access flag obtained is different from the one set", uri, this.url));

        }
        //ctx.attr(AttributeKey.valueOf("type")).set(uri);

        // 可以通过url获取其他参数
        WebSocketServerHandshakerFactory factory = new WebSocketServerHandshakerFactory(
                "ws://" + msg.headers().get("Host") + "/" + this.url + "", null, false);
        handshaker = factory.newHandshaker(msg);
        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        }
        // 进行连接
        handshaker.handshake(ctx.channel(), (FullHttpRequest) msg);
        // 可以做其他处理
        //真正的初始化完成，服务器可以直接发消息，是从这儿开始!!!
//        ctx.writeAndFlush(new TextWebSocketFrame("asdasd"));

        Session session = new Session();
        session.setCtx(ctx);
        wskHandler.onNetInitComplete(session);


    }

    private static void sendHttpResponse(ChannelHandlerContext ctx, FullHttpRequest req, DefaultFullHttpResponse res) {
        // 返回应答给客户端
        if (res.status().code() != 200) {
            ByteBuf buf = Unpooled.copiedBuffer(res.status().toString(), CharsetUtil.UTF_8);
            res.content().writeBytes(buf);
            buf.release();
        }
        // 如果是非Keep-Alive，关闭连接
        ChannelFuture f = ctx.channel().writeAndFlush(res);
        if (!HttpUtil.isKeepAlive(req) || res.status().code() != 200) {
            f.addListener(ChannelFutureListener.CLOSE);
        }
    }

}
