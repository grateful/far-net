package com.far.net.server.core;


import com.far.net.interf.IProcessHandler;
import com.far.net.interf.IWskHandler;
import com.far.net.server.handler.FarChannelHandler;
import com.far.net.server.handler.FarSockChannelHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

import java.nio.ByteOrder;


public class FarSockServer implements Runnable {

    private Integer port;

    private String host;

    private String url;

    private boolean startComplete = false;





    private ChannelInitializer<SocketChannel> channelChannelInitializer;


    public boolean isStartComplete() {
        return startComplete;
    }

    public void setStartComplete(boolean startComplete) {
        this.startComplete = startComplete;
    }

    public ChannelInitializer<SocketChannel> getChannelChannelInitializer() {
        return channelChannelInitializer;
    }

    public FarSockServer setChannelChannelInitializer(ChannelInitializer<SocketChannel> channelChannelInitializer) {
        this.channelChannelInitializer = channelChannelInitializer;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    /**
     * @param host
     * @param port
     * @param url  webscoketURL地址的标识
     * @description:
     * @return:
     * @author mike/Fang.J
     */
    public FarSockServer(String host, int port, String url, ChannelInitializer<SocketChannel> channelChannelInitializer) {
        this.host = host;
        this.port = port;
        this.url = url;
        this.channelChannelInitializer = channelChannelInitializer;

    }


    public void start() {
        new Thread(this).start();
    }

    public void action() {

        int port = this.port;

        EventLoopGroup boss = new NioEventLoopGroup();
        EventLoopGroup work = new NioEventLoopGroup();

        try {
            ServerBootstrap bootstrap = new ServerBootstrap();

            //这里的设置 每次有新的客户端链接上来的时候 都会走一次
            bootstrap.group(boss, work).channel(NioServerSocketChannel.class)
                    .childHandler(this.channelChannelInitializer).option(ChannelOption.SO_BACKLOG, 2048).childOption(ChannelOption.SO_KEEPALIVE, true);

            Channel channel = bootstrap.bind(port).sync().addListener(new GenericFutureListener<Future<? super Void>>() {
                @Override
                public void operationComplete(Future<? super Void> future) throws Exception {
                    synchronized (this){
                        startComplete = true;
                    }

                    System.out.println("netty init ok!");
                }
            }).channel();
            channel.closeFuture().sync();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            boss.shutdownGracefully();
            work.shutdownGracefully();
        }
    }


    @Override
    public void run() {
        action();
    }
}
