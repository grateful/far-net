package com.far.net.server.core;


import com.far.net.interf.IWskAgent;
import com.far.net.interf.IWskHandler;
import com.far.net.server.handler.FarChannelHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

import java.net.SocketException;
import java.rmi.activation.ActivationGroup;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FarNetServer implements Runnable {

    private Integer port;

    private String host;

    private String url;

    private IWskHandler<?> wskHandler;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    /**
     * @param hInf
     * @param wskHandler
     * @description: 参数 "*:*:*" 主机:端口:WSK的访问标识
     * 默认三个参数都要占位 访问标识为*时 默认 wsk
     * ws://0.0.0.1/wsk
     * @return:
     * @author mike/Fang.J/Q184377367
     * @date: 2021/9/7 21:19
     */
    public FarNetServer(String hInf, IWskHandler<?> wskHandler) {

        String[] ha = hInf.split(":");
        host = ha[0];
        port = Integer.valueOf(ha[1]);
        url = ha[2];
        if (url.equals("")) {//错误，必须设置，至少是*号
            throw new Error("No access flags are set");
        }
        url = url.equals("*") ? "wsk" : url;
        this.wskHandler = wskHandler;
    }


    public void start() {
        new Thread(this).start();
    }

    public void action() {

        int port = this.port;

        EventLoopGroup boss = new NioEventLoopGroup();
        EventLoopGroup work = new NioEventLoopGroup();

        try {
            ServerBootstrap bootstrap = new ServerBootstrap();

            //这里的设置 每次有新的客户端链接上来的时候 都会走一次
            bootstrap.group(boss, work).channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {

                            // 心跳检查 必须放在第一个
                            //心跳目前只能在NETTY客户端能实现，WEB上没办法收到心跳包，自己写把
                            //socketChannel.pipeline().addLast(new IdleStateHandler(5,5,60*30, TimeUnit.SECONDS));

                            socketChannel.pipeline().addLast("http-codec", new HttpServerCodec());
                            socketChannel.pipeline().addLast("aggregator", new HttpObjectAggregator(65536));
                            socketChannel.pipeline().addLast("http-chunked", new ChunkedWriteHandler());
                            socketChannel.pipeline().addLast("webSocketHandler", new FarChannelHandler().setUrl(url).setWskHandler(wskHandler));
                            //LOGGER.debug("【Radar】新的终端已连入!");
                            // LogUtilxxx.svrCore.debug("创建新的childHandler成功！");
                        }
                    }).option(ChannelOption.SO_BACKLOG, 2048).childOption(ChannelOption.SO_KEEPALIVE, true);


            Channel channel = bootstrap.bind(port).sync().channel();
//            Channel channel = bootstrap.bind(port).sync().addListener(
//                    new ChannelFutureListener() {
//                        @Override
//                        public void operationComplete(ChannelFuture channelFuture) throws Exception {
//                            Channel deviceChannel;
//                            deviceChannel = channelFuture.channel();
//                            if ( !deviceChannel.isActive() ) {
//                               int x=1;
//                            }
//                            int x =2;
//                           // System.out.println("设备ID[{}]请求注册，channelid:{}",deviceChannel.id().toString(), deviceChannel.id())
//
//                        }
//                    }
//            ).channel();

            channel.closeFuture().sync();

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            boss.shutdownGracefully();
            work.shutdownGracefully();
        }
    }


    @Override
    public void run() {
        action();
    }
}
