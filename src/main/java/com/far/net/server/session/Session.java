package com.far.net.server.session;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;

/*
 * @description: 连接持久包装器
 * @author mike/Fang.J/Q184377367
 * @date 2021/9/7 22:40
*/
public class Session {

    private ChannelHandlerContext ctx;

    public ChannelHandlerContext getCtx() {
        return ctx;
    }

    public void setCtx(ChannelHandlerContext ctx) {
        this.ctx = ctx;
    }

    public String getSeesionId() {
        return ctx.channel().id() + "";
    }

    /**
     * @param text
     * @description:发送消息
     * @return: void
     * @author mike/Fang.J/Q184377367
     */
    public void sendText(String text) {
        //发送的必须是TextWebSocketFrame类型
        this.ctx.writeAndFlush(new TextWebSocketFrame(text));
    }

    /**
     * @description:
     * @param text
     * @return: void
     * @author mike/Fang.J
     */
    public void sendBytes(String text){
        ctx.writeAndFlush(Unpooled.wrappedBuffer(text.getBytes()));
    }

    public void sendBytes(byte[] bytes){
        ctx.writeAndFlush(Unpooled.wrappedBuffer(bytes));
    }

}
