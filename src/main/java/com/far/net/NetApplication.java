package com.far.net;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class NetApplication {

    public static void main(String[] args) {
        SpringApplication.run(NetApplication.class, args);
    }

}
