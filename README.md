# far-net

#### 介绍
生活和工作的节奏越来越快，各种项目、新技术应接不暇，人生苦短，我用farNet。30秒搭建websocket服务端！如果不够快，不够简单，请告诉我们！



#### maven编译好，直接install 引用
```
<groupId>com.far</groupId>
<artifactId>net</artifactId>
<version>0.0.1</version>
```

#### JAR方式引用
- spring boot pom.xml 将jar包放到resources/lib 目录下
```js
  <dependency>
            <groupId>com.far</groupId>
            <artifactId>net</artifactId>
            <version>0.0.1</version>
            <scope>system</scope>
            <systemPath>${pom.basedir}/src/main/resources/lib/far-net-0.0.1.jar</systemPath>
  </dependency>

```
- 依赖包
> <dependency>
    <groupId>io.netty</groupId>
    <artifactId>netty-all</artifactId>
    <version>4.1.5.Final</version>
</dependency>

#### 用例
- src/main/java/demo

#### TODO
- 增加访问标识分组
- 增加JS支持

#### 使用说明

- 使用交流Q群 4915800

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


